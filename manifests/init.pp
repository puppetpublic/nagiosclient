# Installs the client plugins for Nagios monitoring and the remctl
# interface that they use.

# $remctl_class: indicates which remctl Puppet module to load. The legacy
# module is 'base::remctl'. The newer module is 'remctl::server' (available
# at https://code.stanford.edu/pe-public/remctl). Default is 'base::remctl' for
# compatibility's sake.

class nagiosclient (
  Boolean $include_contrib  = false,
  Enum['base::remctl', 'remctl::server']
          $remctl_class     = 'base::remctl',
) {

  case $remctl_class {
    'base::remctl':   { include base::remctl }
    'remctl::server': { include remctl::server }
    default:          { crit('unknown remctl class') }
  }

  case $::osfamily {
    'Debian': {
      # The package "nagios-plugins-basic" changed to a virtual package in
      # Debian buster. Since Puppet package does not support virtual
      # packages with the apt provider, in buster and later we load the
      # non-virtual monitoring-plugins-basic package rather than the
      # virtual package nagios-plugins-basic.
      case $::lsbdistcodename {
        'wheezy', 'jessie', 'stretch': {
          $package = 'nagios-plugins-basic'
        }
        default: {
          $package = 'monitoring-plugins-basic'
        }
      }

      case $::lsbdistcodename {
        'wheezy', 'jessie', 'stretch', 'buster': {
          $contrib_package = 'nagios-plugins-contrib'
        }
        default: {
          $contrib_package = 'monitoring-plugins-contrib'
        }
      }

      if ($include_contrib) {
        package { 'nagios-contrib':
          ensure  => 'present',
          name    => $contrib_package,
          require => Package['stanford-nagios'],
        }
      }
    }
    'RedHat': {
      $package = 'stanford-nagios'
    }
    default: {
      $package = 'nagios-plugins-basic'
    }
  }
  package { 'stanford-nagios':
    ensure  => present,
    name    => $package,
    require => Package['remctl-server'],
  }
  file { '/etc/filter-syslog/nagios-client':
    source => 'puppet:///modules/nagiosclient/etc/filter-syslog/nagios-client',
  }
}
